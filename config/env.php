<?php

use Dotenv\Dotenv as Env;

$path = realpath(__DIR__ . '/..');
$dotenv = Env::create($path);
$dotenv->load();