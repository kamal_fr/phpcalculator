<?php

namespace Config;

use Illuminate\Database\Capsule\Manager as Capsule;
// Set the event dispatcher used by Eloquent models... (optional)
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

class Database {

    public $capsule;

    private $driver;
    private $database;

    public function __construct($connType)
    {
        $this->capsule = new Capsule;

        if($connType == 'mysql'){
            $this->driver = 'mysql';
            $this->database = $_ENV['DB_DATABASE'];
        } else if($connType == 'sqlite'){
            $this->driver = 'sqlite';
            $path = realpath(__DIR__ . '/..');
            $this->database = $path. '/database.sqlite';
        }

        // add connect db
        $this->connect();

        $this->capsule->setEventDispatcher(new Dispatcher(new Container));

        // Make this Capsule instance available globally via static methods... (optional)
        $this->capsule->setAsGlobal();

        // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
        $this->capsule->bootEloquent();
    }

    private function connect()
    {
        $this->capsule->addConnection([
            'driver'    => $this->driver,
            'host'      => $_ENV['DB_HOST'],
            'database'  => $this->database,
            'username'  => $_ENV['DB_USERNAME'],
            'password'  => $_ENV['DB_PASSWORD'],
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);
    }

}