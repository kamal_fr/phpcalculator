<?php

namespace Jakmall\Recruitment\Calculator\History;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CommandHistoryManager implements CommandHistoryManagerInterface {

    public $repository;

    public function __construct(HistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Returns array of command history.
     *
     * @return array
     */
    public function findAll() : array {
        return $this->repository->getAll()->toArray();
    }

    /**
     * Log command data to storage.
     *
     * @param mixed $command The command to log.
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($command): bool {
        return true;
    }

    /**
     * Clear all data from storage.
     *
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clearAll():bool {
        return $this->repository->clearHistoryAll();
    }

    
    public function setDb($conn) : void
    {
        $this->repository->setDb($conn);
    }
    
    public function setFilterCommand(array $commands) : void
    {
        if(count($commands) > 0)
        {
            $this->repository->setFilterCommand($commands);
        }
    }
}