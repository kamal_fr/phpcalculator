<?php

namespace Jakmall\Recruitment\Calculator\History;

use Config\Database;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

class HistoryRepository {
    
    protected $data;
    protected $db;
    
    /**
     * @var Builder
     */
    protected $q;

    public function saveHistory() : void
    {
        $this->storeDataBasedOnDb('mysql');
        $this->storeDataBasedOnDb('sqlite');
    }
    
    public function clearHistoryAll() : bool
    {
        try {
            $this->clearAllBasedOnDb('mysql');
            $this->clearAllBasedOnDb('sqlite');
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    
    /**
     * @param string $db mysql / sqlite
     * @return void
     */
    protected function storeDataBasedOnDb(string $db) : void
    {
        $this->setDb($db);
        $this->storeData();
    }
    
    /**
     * @param string $db mysql / sqlite
     * @return void
     */
    protected function clearAllBasedOnDb(string $db) : void
    {
        $this->setDb($db);
        $this->clearAll();
    }

    /**
     * @param string $connection mysql / sqlite
     *
     * @return void
     */
    protected function storeData() : void
    {
        $result = $this->db->capsule->table('history')->insert([
            'command'       => $this->data['command'],
            'description'   => $this->data['description'],
            'result'        => $this->data['result'],
            'output'        => $this->data['output'],
            'created_at'    => standartDateTime(),
            'updated_at'    => standartDateTime(),
        ]);
    }

    public function setDb(string $connection) {
        $this->db = new Database($connection);
        $this->setQuery();
    }
    
    public function setData(array $data) {
        $this->data = $data;
    }

    public function setQuery()
    {
        $this->q = $this->db->capsule->table('history');
    }

    public function select(array $column = null)
    {
        return $this->q->select($column);
    }

    public function getAll()
    {
        return $this->q->get();
    }
    
    public function clearAll()
    {
        return $this->q->delete();
    }
    
    public function setFilterCommand(array $commands)
    {
        return $this->q->whereIn('command', $commands);
    }

    
}