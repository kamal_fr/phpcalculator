<?php

namespace Jakmall\Recruitment\Calculator\Helper;

use Jakmall\Recruitment\Calculator\History\HistoryRepository;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class BaseHandle
{
    /**
     * @return string $output
     */
    public static function StandartOutput(String $command, String $description, String $result) : String
    {
        $output = sprintf('%s = %s', $description, $result);
        
        $data = [
            'command'       => $command,
            'description'   => $description,
            'result'        => $result,
            'output'        => $output,
        ];

        $repository = new HistoryRepository();
        $repository->setData($data);
        $repository->saveHistory();

        return $output;
    }

    /**
     * Set DB at $historyManager base on $driverValue
     */
    public static function setDBbaseOnDriver(string $driverValue, CommandHistoryManagerInterface $historyManager)
    {
        switch ($driverValue) {
            case 'database':
                $driver = 'mysql';
                break;
            case 'file':
                $driver = 'sqlite';
                break;
            default:
                $driver = 'mysql';
                break;
        }
        $historyManager->setDb($driver);
    }
}