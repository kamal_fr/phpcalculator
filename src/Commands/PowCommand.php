<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Helper\BaseHandle;

class PowCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {base : The base number} {exp : The exponent number}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s the given Number', ucfirst($commandVerb));
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'pow';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'exponent';
    }

    public function handle(): void
    {
        $base = $this->argument('base');
        $exp = $this->argument('exp');
        
        $output = BaseHandle::StandartOutput(
            ucfirst($this->getCommandVerb()),
            $this->generateCalculationDescription($base, $exp),
            $this->calculate($base, $exp)
        );

        $this->comment($output);
    }

    protected function generateCalculationDescription($base, $exp): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return implode($glue, [$base, $exp]);
    }

    protected function getOperator(): string
    {
        return '^';
    }

    /**
     * @param int|float $base
     * @param int|float $exp
     *
     * @return int|float
     */
    protected function calculate($base, $exp)
    {
        return pow($base, $exp);
    }
}
