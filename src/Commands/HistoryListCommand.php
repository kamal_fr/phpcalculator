<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Helper\BaseHandle;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;
    protected $historyManager;

    public function __construct(CommandHistoryManagerInterface $historyManager)
    {
        $this->historyManager = $historyManager;
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s { commands?* : Filter the %s by commands} {--D|driver=database : Driver for storage connection}',
            $commandVerb,
            $this->getCommandCategory()
        );
        $this->description = sprintf('Show calculator %s', $this->getCommandCategory());
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history:list';
    }
    
    protected function getCommandCategory(): string
    {
        return 'history';
    }

    public function handle(): void
    {
        BaseHandle::setDBbaseOnDriver(
            $this->option('driver'),
            $this->historyManager
        );
        $this->historyManager->setFilterCommand($this->getCommands());
        $res = $this->historyManager->findAll();

        if(count($res) == 0){
            $this->info("History is empty");
        } else {
            $this->table($this->getTableHeaders(), $this->getTableContent($res));
        }
    }

    protected function getCommands()
    {
        return $this->argument('commands');
    }
    
    protected function getTableHeaders()
    {
        $headers = ['no','command','description','result','output','time'];
        foreach($headers as &$v){
            $v = ucfirst($v);
        }
        return $headers;
    }

    protected function getTableContent($res)
    {
        $content = [];
        foreach($res as $k => $v){
            array_push($content, [
                'no'        => $k+1,
                'command'   => $v->command,
                'description' => $v->description,
                'result'    => $v->result,
                'output'    => $v->output,
                'time'      => $v->created_at,
            ]);
        }
        return $content;
    }

}
