<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Helper\BaseHandle;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;
    protected $historyManager;

    public function __construct(CommandHistoryManagerInterface $historyManager)
    {
        $this->historyManager = $historyManager;
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s',
            $commandVerb
        );
        $this->description = sprintf('Clear saved %s', $this->getCommandCategory());
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history:clear';
    }
    
    protected function getCommandCategory(): string
    {
        return 'history';
    }

    public function handle(): void
    {
        $clear = $this->historyManager->clearAll();

        if($clear){
            $this->info("History cleared!");
        }
    }

}
