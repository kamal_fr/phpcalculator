<?php

namespace Jakmall\Recruitment\Calculator\Migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;

class CreateHistoryTable {

    public $capsule;
    private $tableName = 'history';

    function __construct(Capsule $capsule)
    {
        $this->capsule = $capsule;
    }

    /** create table */
    public function up()
    {
        if(!$this->capsule->schema()->hasTable($this->tableName))
        {
            $this->capsule->schema()->create($this->tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->string('command');
                $table->string('description');
                $table->string('result');
                $table->string('output');
                $table->timestamps();
            });
        }
    }

    /** delete table */
    public function down(){
        $this->capsule->schema()->dropIfExists($this->tableName);
    }
}

