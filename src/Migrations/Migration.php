<?php

namespace Jakmall\Recruitment\Calculator\Migrations;

use Config\Database;
use Jakmall\Recruitment\Calculator\Migrations\CreateHistoryTable;

class Migration {

    public $driver;

    /**
     * @param string $driver driver can mysql / sqlite / both 
     */
    function __construct($driver)
    {
        $this->driver = $driver;
    }

    
    /** 
     * run migration 
    */
    public function run()
    { 
        switch ($this->driver) {
            case 'mysql':
            case 'sqlite':
                $this->runMigrateDB($this->driver);
                break;
            case 'both':
                $this->runMigrateDB('mysql');
                $this->runMigrateDB('sqlite');
                break;
            default:
                break;
        }
    }

    /**
     * @param string $type value must mysql / sqlite 
     */
    public function runMigrateDB($type)
    { 
        $db = new Database($type);
        $this->CreateMigration($db);
    }

    /** 
     * @param Database $db database of object
     */
    public function CreateMigration(Database $db)
    {
        foreach($this->list() as $m){
            $migration = new $m($db->capsule);
            $migration->up();
        }
    }

    /** 
     * @return array class
     */
    public function list()
    {
        return [CreateHistoryTable::class];
    }
}

